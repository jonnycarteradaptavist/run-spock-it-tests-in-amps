The goal of this repository is to reproduce the problem described in https://ecosystem.atlassian.net/browse/AMPS-1553 / https://community.developer.atlassian.com/t/advanced-maven-failsafe-configurations-plus-amps/42302/9 and test various solutions.

To reproduce the problem, run `mvn clean verify`. Note that the `target/failsafe-reports/` directory ends up empty.
