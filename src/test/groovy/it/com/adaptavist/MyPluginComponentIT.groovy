package it.com.adaptavist

import spock.lang.Specification

class MyPluginComponentIT extends Specification {
    def "test"() {
        when:
        def x = 1

        then:
        x < 2
    }
}
